var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StationSchema = new Schema({
    name: { type: String, require: true },
    coordinates: { latitude: String, longitude: String },
    town: { type: String },
    products: [{ name: String, price: Number }],
    notices: [{ message: String, expiryDate: Date }],
    region: { type: String }
});

module.exports = mongoose.model('Stations', StationSchema);

