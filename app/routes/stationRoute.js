var Station = require('../models/station');

module.exports = function (app, express) {

    var api = express.Router();

    api.post('/sample', function (req, res) {
        var station = new Station();

        station.name = 'Shell Station';
        station.products = { name: 'Petrol', price: 3.5 };

        console.log('station created');
        station.save(function (err) {
            if (err) {
                return res.send(err);
            }
            else {
                res.json({ success: true, message: "New Station Created Successfully" });
            }
        });

    });

    api.get('/stations/query', function (req, res) {
        var page = parseInt(req.query.page),
            size = parseInt(req.query.size),
            skip = page > 0 ? ((page - 1) * size) : 0,
            recordCount=0;
            Station.find(function (err, stations) {
                if(stations){this.recordCount=stations.length;
                console.log('recordcount:' ,this.recordCount);}
            });

        Station.find(null, null, {
            skip: skip,
            limit: size
        }, function (err, data) {
            if (err) {
                res.json(500, err);
            }
            else {
                res.json({
                    success: true,
                    data: data,
                    total:this.recordCount,
                    message: "record found"
                });
            }
        });

    });

    api.route('/stations')
        //get all stations
        .get(function (req, res) {
            Station.find(function (err, stations) {
                if (err) res.send(err);

                res.json({
                    success: true,
                    data: stations,
                    total: stations.length,
                    message: "record found"
                });
            });
        })
        //create a station
        .post(function (req, res) {


            Station.findOne({ station: req.body.name })
                .select('name').exec(function (err, prof) {
                    if (!prof) {
                        var station = new Station();

                        station.name = req.body.name;
                        station.coordinates = req.body.coordinates;
                        station.products = req.body.products;
                        station.notices = req.body.notices;
                        station.region = req.body.region;
                        station.town = req.body.town;

                        station.save(function (err) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.json({
                                    success: true,
                                    data: [],
                                    total: 1,
                                    message: 'New Station Saved'
                                });
                            }
                        });

                    } else if (prof) {
                        res.json({
                            success: false,
                            message: "Station already exist"
                        });
                    }
                });
        });

    api.route('/stations/:id')
        .get(function (req, res) {
            Station.findById(req.params.id, function (err, station) {

                if (!station || err) {
                    // res.json({
                    //     success: false,
                    //     data: "",
                    //     message: "Unable to find station",
                    //     total: 0
                    // });
                    res.send(err);
                }
                else {

                    res.json({
                        success: true,
                        data: station,
                        total: 1,
                        message: "Record found"
                    });
                }
            });
        })
        .put(function (req, res) {
            Station.findById(req.params.id, function (err, station) {
                if (err) {
                    // res.json({
                    //     success: false,
                    //     message: "Unable to update station",
                    //     total: 0,
                    //     data: ""
                    // });
                    res.send(err)
                }
                else {

                    if (req.body.name) station.name = req.body.name;
                    if (req.body.coordinates) station.coordinates = req.body.coordinates;
                    if (req.body.coordinates) station.products = req.body.products;
                    if (req.body.coordinates) station.notices = req.body.notices;
                    if (req.body.region) station.region = req.body.region;
                    if (req.body.town) station.town = req.body.town;

                    station.save(function (err) {
                        if (err) res.send(err);

                        //return a message
                        res.json({ success: true, data: [], message: 'Station updated', total: 1 });
                    });
                }
            });
        })
        .delete(function (req, res) {
            Station.remove({
                _id: req.params.id
            }, function (err, station) {
                if (err) return res.send(err);
                res.json({ success: true, data: [], message: 'Station successfully deleted', total: 1 });

            });
        });



    return api;

};
