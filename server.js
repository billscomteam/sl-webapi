var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),//used to see request
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    config = require('./config'),
    path = require('path');

   //use body parser to get information from POST request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//configure our app to handle CORS request (Cross-Origin Resource Sharing)
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-Width,content-type, Authorization');
    next();
});

//log all requests to the console
app.use(morgan('dev'));

// connect to db
mongoose.connect(config.database);

//set static files location
//used for requests that our frontend will make
app.use(express.static(__dirname+'/public'));

// register routes

var stationRoute=require('./app/routes/stationRoute')(app,express);
app.use('/api',stationRoute);

app.listen(config.port);
console.log('fuel-locator started on port ' +config.port);
